from coreapp import app
from flask import request
import datetime

@app.route('/')

def index():
	page = """
	<!DOCTYPE html>
	<html lang = "en">
		<head>
			<meta charset = "utf-8">
			<title>Date of the Week</title>
		</head>
		<body>
			<h1>Welcome to Moustafa's page, please enter a Date</h1>
			<p>Select a date and we will find the day of the week for you</p>

		<form action = "/dow" method = "get">
		<label for = "day">Day:</label>
		<select  name = "day" id = "day">
			<option value = "1">1</option>
			<option value = "2">2</option>
			<option value = "3">3</option>
			<option value = "4">4</option>
			<option value = "5">5</option>
			<option value = "6">6</option>
			<option value = "7">7</option>
			<option value = "8">8</option>
			<option value = "9">9</option>
			<option value = "10">10</option>
			<option value = "11">11</option>
			<option value = "12">12</option>
			<option value = "13">13</option>
			<option value = "14">14</option>
			<option value = "15">15</option>
			<option value = "16">16</option>
			<option value = "17">17</option>
			<option value = "18">18</option>
			<option value = "19">19</option>
			<option value = "20">20</option>
			<option value = "21">21</option>
			<option value = "22">22</option>
			<option value = "23">23</option>
			<option value = "24">24</option>
			<option value = "25">25</option>
			<option value = "26">26</option>
			<option value = "27">27</option>
			<option value = "28">28</option>
			<option value = "29">29</option>
			<option value = "30">30</option>
			<option value = "31">31</option>
		</select>

		<label for = "month">Month:</label>
		<select name = "month" id = "month">
			<option value = "1">1</option>
			<option value = "2">2</option>
			<option value = "3">3</option>
			<option value = "4">4</option>
			<option value = "5">5</option>
			<option value = "6">6</option>
			<option value = "7">7</option>
			<option value = "8">8</option>
			<option value = "9">9</option>
			<option value = "10">10</option>
			<option value = "11">11</option>
			<option value = "12">12</option>
		
		</select>
		<label for = "year">Year:</label>
		<select name = "year" id = "yaer">
			<option value = "2014">2014</option>
			<option value = "2015">2015</option>
			<option value = "2016">2016</option>
			<option value = "2017">2017</option>
			<option value = "2018">2018</option>
			<option value = "2019">2019</option>
			<option value = "2020">2020</option>
			<option value = "2021">2021</option>
			<option value = "2022">2022</option>
		</select>
		<input type = "submit" value = "Submit Query">
		</form>
		"""
	return page

@app.route('/dow')

def dow():
	
	day = int(request.args.getlist('day')[0])
	month = int(request.args.getlist('month')[0])
	year = int(request.args.getlist('year')[0])

	try:
		dow = datetime.date(year, month, day).strftime("%A")
	except ValueError as err:
		print(err)

	page1 = """<!DOCTYPE html>
		<html lang = "en">
		<head>
			<meta charset = "utf-8">
			<title>Day of the week</title>
		</head>

		<body>
			<h1>Day of the week</h1>
			<p>The date you choose is <strong>%s</strong></p>
		</body>
		</html>
		"""%dow
	return page1

